package com.magnet.entities.local;

import com.magnet.persistence.android.EntityCollection;

public class EntityContext extends com.magnet.persistence.android.EntityContext {

  public EntityContext(com.magnet.persistence.EntityContext context) {
    super(context);
  }

  /**
  * Get the entity collection for the Office entity
  */
  public EntityCollection<Office> getOffices() {
    return new EntityCollection<Office>(this.getCollection(Office.class, "Offices"));
  }

  /**
  * Get the entity collection for the Building entity
  */
  public EntityCollection<Building> getBuildings() {
    return new EntityCollection<Building>(this.getCollection(Building.class, "Buildings"));
  }

}
