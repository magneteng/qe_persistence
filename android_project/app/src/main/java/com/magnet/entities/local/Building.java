package com.magnet.entities.local;
import com.magnet.model.beans.ObjectFactory;
import com.magnet.model.beans.ObjectProperty;
import com.magnet.model.beans.ValueReference;
import com.magnet.model.beans.ObjectProxyCallback;
import com.magnet.model.beans.ObjectFactoryReference;
import com.magnet.model.beans.spi.AbstractObjectFactory;
import com.magnet.model.reflection.Type;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.InvalidClassException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.UUID;
import com.magnet.persistence.RelativeCollection;

public class Building implements Serializable {
  static {
    try {
      ObjectFactory.register(new BuildingFactory());
    } catch (NoSuchMethodException ex) {
      throw new Error("This cannot happen", ex);
    }
  }

  private UUID id;
  private String name;
  private RelativeCollection<Office> offices;

  /**
  * Sets the id property
  * @param value The value of the id property
  */
  public void setId(UUID value) {
    this.id = value;
  }

  /**
  * @return The value of the id property
  */
  public UUID getId() {
    return id;
  }

  /**
  * Sets the name property
  * @param value The value of the name property
  */
  public void setName(String value) {
    this.name = value;
  }

  /**
  * @return The value of the name property
  */
  public String getName() {
    return name;
  }

  /**
  * @return The value of the offices property
  */
  public RelativeCollection<Office> getOffices() {
    return offices;
  }

  /**
  * Convert the instance to string
  */
  public String toString() {
    return "Building { " + "id : " + id + " }";
  }

  private Object[] hashCodeProperties() {
    return new Object[] { id, Building.class };
  }

  /**
  * @return hash code of the instance
  */
  public int hashCode() {
    return Arrays.deepHashCode(hashCodeProperties());
  }

  private Object[] equalsProperties() {
    return new Object[] { id };
  }

  /**
  * @return true if two instances are equal to each other; otherwise false
  */
  public boolean equals(Object paramObject) {
    return ((paramObject instanceof Building)) && ((this == paramObject) || (Arrays.deepEquals(equalsProperties(), ((Building)paramObject).equalsProperties())));
  }

  /**
  * Use in query for the property id
  */
  public static final String ID = "id";

  /**
  * Use in query for the property name
  */
  public static final String NAME = "name";


  /**
  * @param property The property
  * @return the value of the property
  */
  Object get(Property property) {
    switch (property) {
      case id:
        return getId();
      case name:
        return getName();
      case offices:
        return getOffices();
      default:
        throw new Error("unknown property: " + property);
    }
  }

  /**
  * Sets the value of the property
  * @param property The property
  * @param value The value of the property
  */
  void set(Property property, Object value) {
    switch (property) {
      case id:
        setId((UUID) value);
        break;
      case name:
        setName((String) value);
        break;
      case offices:
        this.offices = (RelativeCollection<Office>) value;
        break;
      default:
        throw new Error("unknown property: " + property);
    }
  }

  /**
  * @param property The property
  * @return the value reference of the property
  */
  ValueReference<?> getReference(Property property) {
    throw new Error("unknown property: ");
  }

  /**
  * Sets the value reference of the property
  * @param property The property
  * @param value The value reference of the property
  */
  void setReference(Property property, ValueReference<?> value) {
    throw new Error("unknown property: ");
  }


  /**
  * The Building Factory
  */
  private static class BuildingFactory extends AbstractObjectFactory<Building> {

    public BuildingFactory() throws NoSuchMethodException {
      super(null, Property.class.getEnumConstants());
    }

    @Override
    public Type<Building> getType() {
      return Type.from(Building.class);
    }

    @Override
    public Building create() {
      return new Building();
    }

    @Override
    public Building createProxy(ObjectProxyCallback callback) {
      return new BuildingProxy(this, callback);
    }

    @Override
    public boolean supportsProxies() {
      return true;
    }

    @Override
    public ObjectProxyCallback getProxyCallback(Building proxy) throws UnsupportedOperationException, IllegalArgumentException {
      return ((BuildingProxy) proxy).callback;
    }
  }


  /**
  * The Proxy of Building
  */
  @ObjectFactoryReference("com.magnet.entities.local.Building")
  private static class BuildingProxy extends Building {

    private final ObjectProxyCallback callback;
    private final ObjectFactory<?> factory;

    public BuildingProxy(ObjectFactory<?> factory, ObjectProxyCallback callback) {
      this.callback = callback;
      this.factory = factory;
    }

    private <T> T callback(String property) {
      return (T)callback.onPropertyInvoked(this, factory.getProperty(property));
    }
    

    public ObjectProxyCallback getCallback() {
      return callback;
    }
    

    @Override
    public UUID getId() {
      return callback("id");
    }

    @Override
    public void setId(UUID value) {
      throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
      return callback("name");
    }

    @Override
    public void setName(String value) {
      throw new UnsupportedOperationException();
    }

    @Override
    public RelativeCollection<Office> getOffices() {
      return callback("offices");
    }

  }


  /**
  * The Property Factory
  */
  private static enum Property implements ObjectProperty {

    id(Type.from(UUID.class), "id", false, "getId", "setId", new Class<?>[]{UUID.class}),
    name(Type.from(String.class), "name", false, "getName", "setName", new Class<?>[]{String.class}),
    offices(Type.build(RelativeCollection.class, Office.class), "offices", false, "getOffices", null, new Class<?>[]{RelativeCollection.class});

    private final String __name;
    private final Type<?> __propertyType;
    private final boolean __supportsReferences;
    private final Method __getGetMethodBinding;
    private final Method __getSetMethodBinding;

    Property(Type<?> propertyType, String name, boolean supportsReferences, String getMethodName, String setMethodName, Class<?>... args) {
      this.__name = name;
      this.__propertyType = propertyType;
      this.__supportsReferences = supportsReferences;
      this.__getGetMethodBinding = getMethodName != null ? getMethodBinding(getMethodName, new Class<?>[0]) : null;
      this.__getSetMethodBinding = setMethodName != null ? getMethodBinding(setMethodName, args) : null;
    }

    private Method getMethodBinding(String methodName, Class<?>... args) {
      try {
        return Building.class.getMethod(methodName, args);
      } catch (NoSuchMethodException ex) {
        throw new Error("Unable to find " + methodName + ". " + ex);
      }
    }

    @Override
    public String getName() {
      return __name;
    }

    @Override
    public Object get(Object instance) {
      return ((Building)instance).get(this);
    }

    @Override
    public void set(Object instance, Object value) {
      ((Building)instance).set(this, value);
    }

    @Override
    public String getDescription() {
      return null;
    }

    @Override
    public Method getGetMethodBinding() {
      return __getGetMethodBinding;
    }

    @Override
    public Type getType() {
       return __propertyType; 
    }

    @Override
    public boolean supportsReferences() {
      return __supportsReferences;
    }

    @Override
    public ValueReference<?> getReference(Object instance) {
      return ((Building)instance).getReference(this);
    }

    @Override
    public void setReference(Object instance, ValueReference<?> value) {
      ((Building)instance).setReference(this, value);
    }

    @Override
    public Method getSetMethodBinding() {
      return __getSetMethodBinding;
    }
  }

  private static final UUID versionUUID = java.util.UUID.fromString("49aa1715-62e4-430a-9fd4-646ed066d8e3");

  private void writeObject(ObjectOutputStream out) throws IOException {
    out.writeObject(versionUUID);
    out.writeObject(getId());
    out.writeObject(getName());
  }

  private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
    if (!versionUUID.equals((UUID)in.readObject())) {
      throw new InvalidClassException("Building version changed.");
    }
    setId((UUID)in.readObject());
    setName((String)in.readObject());
  }

}
