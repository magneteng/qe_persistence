package com.magnet.entities.remote;
import com.magnet.model.beans.ObjectFactory;
import com.magnet.model.beans.ObjectProperty;
import com.magnet.model.beans.ValueReference;
import com.magnet.model.beans.ObjectProxyCallback;
import com.magnet.model.beans.ObjectFactoryReference;
import com.magnet.model.beans.spi.AbstractObjectFactory;
import com.magnet.model.reflection.Type;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.InvalidClassException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.UUID;

public class Employee implements Serializable {
  static {
    try {
      ObjectFactory.register(new EmployeeFactory());
    } catch (NoSuchMethodException ex) {
      throw new Error("This cannot happen", ex);
    }
  }

  private String name;
  private UUID id;
  private ValueReference<Department> department = ValueReference.createDefault(Department.class);

  /**
  * Sets the name property
  * @param value The value of the name property
  */
  public void setName(String value) {
    this.name = value;
  }

  /**
  * @return The value of the name property
  */
  public String getName() {
    return name;
  }

  /**
  * Sets the id property
  * @param value The value of the id property
  */
  public void setId(UUID value) {
    this.id = value;
  }

  /**
  * @return The value of the id property
  */
  public UUID getId() {
    return id;
  }

  /**
  * Sets the department property
  * @param value The value of the department property
  */
  public void setDepartment(Department value) {
    this.department.set(value);
  }

  /**
  * @return The value of the department property
  */
  public Department getDepartment() {
    return department!= null ? department.get() : null;
  }

  /**
  * Convert the instance to string
  */
  public String toString() {
    return "Employee { " + "id : " + id + " }";
  }

  private Object[] hashCodeProperties() {
    return new Object[] { id, Employee.class };
  }

  /**
  * @return hash code of the instance
  */
  public int hashCode() {
    return Arrays.deepHashCode(hashCodeProperties());
  }

  private Object[] equalsProperties() {
    return new Object[] { id };
  }

  /**
  * @return true if two instances are equal to each other; otherwise false
  */
  public boolean equals(Object paramObject) {
    return ((paramObject instanceof Employee)) && ((this == paramObject) || (Arrays.deepEquals(equalsProperties(), ((Employee)paramObject).equalsProperties())));
  }

  /**
  * Use in query for the property name
  */
  public static final String NAME = "name";

  /**
  * Use in query for the property id
  */
  public static final String ID = "id";

  /**
  * Use in query for the property department
  */
  public static final String DEPARTMENT = "department.";


  /**
  * @param property The property
  * @return the value of the property
  */
  Object get(Property property) {
    switch (property) {
      case name:
        return getName();
      case id:
        return getId();
      case department:
        return getDepartment();
      default:
        throw new Error("unknown property: " + property);
    }
  }

  /**
  * Sets the value of the property
  * @param property The property
  * @param value The value of the property
  */
  void set(Property property, Object value) {
    switch (property) {
      case name:
        setName((String) value);
        break;
      case id:
        setId((UUID) value);
        break;
      case department:
        setDepartment((Department) value);
        break;
      default:
        throw new Error("unknown property: " + property);
    }
  }

  /**
  * @param property The property
  * @return the value reference of the property
  */
  ValueReference<?> getReference(Property property) {
    switch (property) {
      case department:
        return department;
      default:
          throw new Error("unknown property: " + property);
    }
  }

  /**
  * Sets the value reference of the property
  * @param property The property
  * @param value The value reference of the property
  */
  void setReference(Property property, ValueReference<?> value) {
    switch (property) {
      case department:
        department = (ValueReference<Department>) value;
        break;
      default:
          throw new Error("unknown property: " + property);
    }
  }


  /**
  * The Employee Factory
  */
  private static class EmployeeFactory extends AbstractObjectFactory<Employee> {

    public EmployeeFactory() throws NoSuchMethodException {
      super(null, Property.class.getEnumConstants());
    }

    @Override
    public Type<Employee> getType() {
      return Type.from(Employee.class);
    }

    @Override
    public Employee create() {
      return new Employee();
    }

    @Override
    public Employee createProxy(ObjectProxyCallback callback) {
      return new EmployeeProxy(this, callback);
    }

    @Override
    public boolean supportsProxies() {
      return true;
    }

    @Override
    public ObjectProxyCallback getProxyCallback(Employee proxy) throws UnsupportedOperationException, IllegalArgumentException {
      return ((EmployeeProxy) proxy).callback;
    }
  }


  /**
  * The Proxy of Employee
  */
  @ObjectFactoryReference("com.magnet.entities.remote.Employee")
  private static class EmployeeProxy extends Employee {

    private final ObjectProxyCallback callback;
    private final ObjectFactory<?> factory;

    public EmployeeProxy(ObjectFactory<?> factory, ObjectProxyCallback callback) {
      this.callback = callback;
      this.factory = factory;
    }

    private <T> T callback(String property) {
      return (T)callback.onPropertyInvoked(this, factory.getProperty(property));
    }
    

    public ObjectProxyCallback getCallback() {
      return callback;
    }
    

    @Override
    public String getName() {
      return callback("name");
    }

    @Override
    public void setName(String value) {
      throw new UnsupportedOperationException();
    }

    @Override
    public UUID getId() {
      return callback("id");
    }

    @Override
    public void setId(UUID value) {
      throw new UnsupportedOperationException();
    }

    @Override
    public Department getDepartment() {
      return callback("department");
    }

    @Override
    public void setDepartment(Department value) {
      throw new UnsupportedOperationException();
    }

  }


  /**
  * The Property Factory
  */
  private static enum Property implements ObjectProperty {

    name(Type.from(String.class), "name", false, "getName", "setName", new Class<?>[]{String.class}),
    id(Type.from(UUID.class), "id", false, "getId", "setId", new Class<?>[]{UUID.class}),
    department(Type.from(Department.class), "department", true, "getDepartment", "setDepartment", new Class<?>[]{Department.class});

    private final String __name;
    private final Type<?> __propertyType;
    private final boolean __supportsReferences;
    private final Method __getGetMethodBinding;
    private final Method __getSetMethodBinding;

    Property(Type<?> propertyType, String name, boolean supportsReferences, String getMethodName, String setMethodName, Class<?>... args) {
      this.__name = name;
      this.__propertyType = propertyType;
      this.__supportsReferences = supportsReferences;
      this.__getGetMethodBinding = getMethodName != null ? getMethodBinding(getMethodName, new Class<?>[0]) : null;
      this.__getSetMethodBinding = setMethodName != null ? getMethodBinding(setMethodName, args) : null;
    }

    private Method getMethodBinding(String methodName, Class<?>... args) {
      try {
        return Employee.class.getMethod(methodName, args);
      } catch (NoSuchMethodException ex) {
        throw new Error("Unable to find " + methodName + ". " + ex);
      }
    }

    @Override
    public String getName() {
      return __name;
    }

    @Override
    public Object get(Object instance) {
      return ((Employee)instance).get(this);
    }

    @Override
    public void set(Object instance, Object value) {
      ((Employee)instance).set(this, value);
    }

    @Override
    public String getDescription() {
      return null;
    }

    @Override
    public Method getGetMethodBinding() {
      return __getGetMethodBinding;
    }

    @Override
    public Type getType() {
       return __propertyType; 
    }

    @Override
    public boolean supportsReferences() {
      return __supportsReferences;
    }

    @Override
    public ValueReference<?> getReference(Object instance) {
      return ((Employee)instance).getReference(this);
    }

    @Override
    public void setReference(Object instance, ValueReference<?> value) {
      ((Employee)instance).setReference(this, value);
    }

    @Override
    public Method getSetMethodBinding() {
      return __getSetMethodBinding;
    }
  }

  private static final UUID versionUUID = java.util.UUID.fromString("912f2cc7-0a7c-4407-aa96-38a5164aec6a");

  private void writeObject(ObjectOutputStream out) throws IOException {
    out.writeObject(versionUUID);
    out.writeObject(getName());
    out.writeObject(getId());
    out.writeObject(getDepartment());
  }

  private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
    if (!versionUUID.equals((UUID)in.readObject())) {
      throw new InvalidClassException("Employee version changed.");
    }
    setName((String)in.readObject());
    setId((UUID)in.readObject());
    if (this.department== null) {
      department= ValueReference.createDefault(Department.class);
    }
    setDepartment((Department)in.readObject());
  }

}
