package com.magnet.entities.local;
import com.magnet.model.beans.ObjectFactory;
import com.magnet.model.beans.ObjectProperty;
import com.magnet.model.beans.ValueReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import com.magnet.persistence.RelativeCollection;

public class BuildingBuilder {

  private final Map<ObjectProperty, Object> values = new HashMap<ObjectProperty, Object>();
  private final ObjectFactory<Building> factory = ObjectFactory.get(Building.class);

  /**
   * Resets the set values in this builder
   */
  public void reset() {
    values.clear();
  }

  /**
   * Builds the target object given the values of the properties specified
  * @return The target object
   */
  public Building build() {
    Building instance = factory.create();
    for(Entry<ObjectProperty, Object> entry : values.entrySet()) {
      if(entry.getValue() instanceof ValueReference) {
        entry.getKey().setReference(instance, (ValueReference)entry.getValue());
      } else {
        entry.getKey().set(instance, entry.getValue());
      }
    }
    return instance;
  }

  /**
   * Sets the value of the id property
   * @param value The value of the property
   * @return The builder instance
   */
  public BuildingBuilder id(UUID value) {
    values.put(factory.getProperty("id"), value);
    return this;
  }

  /**
   * Sets the value of the name property
   * @param value The value of the property
   * @return The builder instance
   */
  public BuildingBuilder name(String value) {
    values.put(factory.getProperty("name"), value);
    return this;
  }

}
