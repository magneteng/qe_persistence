package com.magnet.entities.local;

import android.content.Context;
import java.io.InputStream;
import java.sql.SQLException;
import com.magnet.persistence.EntityContextType;
import com.magnet.persistence.services.ServiceProvider;

public class AndroidDataClient extends com.magnet.persistence.android.AndroidDataClient {

  public AndroidDataClient(Context context) throws SQLException {
    super(context, null, null);
  }

  public AndroidDataClient(Context context, String endpoint, InputStream manifest) throws SQLException {
    super(context, endpoint, manifest);
  }

  public EntityContext createEntityContext() {
    return new EntityContext(getEntityContextDefinition().createContext());
  }

  public EntityContext createEntityContext(EntityContextType type) {
    return new EntityContext(getEntityContextDefinition().createContext(type));
  }

  public EntityContext createEntityContext(EntityContextType type, ServiceProvider serviceProvider) {
    return new EntityContext(getEntityContextDefinition().createContext(type, serviceProvider));
  }

}
