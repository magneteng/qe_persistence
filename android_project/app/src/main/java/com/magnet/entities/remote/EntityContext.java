package com.magnet.entities.remote;

import com.magnet.persistence.android.EntityCollection;

public class EntityContext extends com.magnet.persistence.android.EntityContext {

  public EntityContext(com.magnet.persistence.EntityContext context) {
    super(context);
  }

  /**
  * Get the entity collection for the Employee entity
  */
  public EntityCollection<Employee> getEmployees() {
    return new EntityCollection<Employee>(this.getCollection(Employee.class, "Employees"));
  }

  /**
  * Get the entity collection for the Department entity
  */
  public EntityCollection<Department> getDepartments() {
    return new EntityCollection<Department>(this.getCollection(Department.class, "Departments"));
  }

}
