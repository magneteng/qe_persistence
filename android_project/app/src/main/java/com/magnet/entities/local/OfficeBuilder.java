package com.magnet.entities.local;
import com.magnet.model.beans.ObjectFactory;
import com.magnet.model.beans.ObjectProperty;
import com.magnet.model.beans.ValueReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

public class OfficeBuilder {

  private final Map<ObjectProperty, Object> values = new HashMap<ObjectProperty, Object>();
  private final ObjectFactory<Office> factory = ObjectFactory.get(Office.class);

  /**
   * Resets the set values in this builder
   */
  public void reset() {
    values.clear();
  }

  /**
   * Builds the target object given the values of the properties specified
  * @return The target object
   */
  public Office build() {
    Office instance = factory.create();
    for(Entry<ObjectProperty, Object> entry : values.entrySet()) {
      if(entry.getValue() instanceof ValueReference) {
        entry.getKey().setReference(instance, (ValueReference)entry.getValue());
      } else {
        entry.getKey().set(instance, entry.getValue());
      }
    }
    return instance;
  }

  /**
   * Sets the value of the building property
   * @param value The value of the property
   * @return The builder instance
   */
  public OfficeBuilder building(Building value) {
    values.put(factory.getProperty("building"), value);
    return this;
  }

   /** 
   * Sets the value reference of the building property
   * @param value The value reference of the property
   * @return The builder instance
   */
  public OfficeBuilder building(ValueReference<Building> value) {
    values.put(factory.getProperty("building"), value);
    return this;
  }

  /**
   * Sets the value of the id property
   * @param value The value of the property
   * @return The builder instance
   */
  public OfficeBuilder id(UUID value) {
    values.put(factory.getProperty("id"), value);
    return this;
  }

  /**
   * Sets the value of the name property
   * @param value The value of the property
   * @return The builder instance
   */
  public OfficeBuilder name(String value) {
    values.put(factory.getProperty("name"), value);
    return this;
  }

}
