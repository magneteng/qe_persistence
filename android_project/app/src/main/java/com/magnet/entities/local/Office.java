package com.magnet.entities.local;
import com.magnet.model.beans.ObjectFactory;
import com.magnet.model.beans.ObjectProperty;
import com.magnet.model.beans.ValueReference;
import com.magnet.model.beans.ObjectProxyCallback;
import com.magnet.model.beans.ObjectFactoryReference;
import com.magnet.model.beans.spi.AbstractObjectFactory;
import com.magnet.model.reflection.Type;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.InvalidClassException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.UUID;

public class Office implements Serializable {
  static {
    try {
      ObjectFactory.register(new OfficeFactory());
    } catch (NoSuchMethodException ex) {
      throw new Error("This cannot happen", ex);
    }
  }

  private ValueReference<Building> building = ValueReference.createDefault(Building.class);
  private UUID id;
  private String name;

  /**
  * Sets the building property
  * @param value The value of the building property
  */
  public void setBuilding(Building value) {
    this.building.set(value);
  }

  /**
  * @return The value of the building property
  */
  public Building getBuilding() {
    return building!= null ? building.get() : null;
  }

  /**
  * Sets the id property
  * @param value The value of the id property
  */
  public void setId(UUID value) {
    this.id = value;
  }

  /**
  * @return The value of the id property
  */
  public UUID getId() {
    return id;
  }

  /**
  * Sets the name property
  * @param value The value of the name property
  */
  public void setName(String value) {
    this.name = value;
  }

  /**
  * @return The value of the name property
  */
  public String getName() {
    return name;
  }

  /**
  * Convert the instance to string
  */
  public String toString() {
    return "Office { " + "id : " + id + " }";
  }

  private Object[] hashCodeProperties() {
    return new Object[] { id, Office.class };
  }

  /**
  * @return hash code of the instance
  */
  public int hashCode() {
    return Arrays.deepHashCode(hashCodeProperties());
  }

  private Object[] equalsProperties() {
    return new Object[] { id };
  }

  /**
  * @return true if two instances are equal to each other; otherwise false
  */
  public boolean equals(Object paramObject) {
    return ((paramObject instanceof Office)) && ((this == paramObject) || (Arrays.deepEquals(equalsProperties(), ((Office)paramObject).equalsProperties())));
  }

  /**
  * Use in query for the property building
  */
  public static final String BUILDING = "building.";

  /**
  * Use in query for the property id
  */
  public static final String ID = "id";

  /**
  * Use in query for the property name
  */
  public static final String NAME = "name";


  /**
  * @param property The property
  * @return the value of the property
  */
  Object get(Property property) {
    switch (property) {
      case building:
        return getBuilding();
      case id:
        return getId();
      case name:
        return getName();
      default:
        throw new Error("unknown property: " + property);
    }
  }

  /**
  * Sets the value of the property
  * @param property The property
  * @param value The value of the property
  */
  void set(Property property, Object value) {
    switch (property) {
      case building:
        setBuilding((Building) value);
        break;
      case id:
        setId((UUID) value);
        break;
      case name:
        setName((String) value);
        break;
      default:
        throw new Error("unknown property: " + property);
    }
  }

  /**
  * @param property The property
  * @return the value reference of the property
  */
  ValueReference<?> getReference(Property property) {
    switch (property) {
      case building:
        return building;
      default:
          throw new Error("unknown property: " + property);
    }
  }

  /**
  * Sets the value reference of the property
  * @param property The property
  * @param value The value reference of the property
  */
  void setReference(Property property, ValueReference<?> value) {
    switch (property) {
      case building:
        building = (ValueReference<Building>) value;
        break;
      default:
          throw new Error("unknown property: " + property);
    }
  }


  /**
  * The Office Factory
  */
  private static class OfficeFactory extends AbstractObjectFactory<Office> {

    public OfficeFactory() throws NoSuchMethodException {
      super(null, Property.class.getEnumConstants());
    }

    @Override
    public Type<Office> getType() {
      return Type.from(Office.class);
    }

    @Override
    public Office create() {
      return new Office();
    }

    @Override
    public Office createProxy(ObjectProxyCallback callback) {
      return new OfficeProxy(this, callback);
    }

    @Override
    public boolean supportsProxies() {
      return true;
    }

    @Override
    public ObjectProxyCallback getProxyCallback(Office proxy) throws UnsupportedOperationException, IllegalArgumentException {
      return ((OfficeProxy) proxy).callback;
    }
  }


  /**
  * The Proxy of Office
  */
  @ObjectFactoryReference("com.magnet.entities.local.Office")
  private static class OfficeProxy extends Office {

    private final ObjectProxyCallback callback;
    private final ObjectFactory<?> factory;

    public OfficeProxy(ObjectFactory<?> factory, ObjectProxyCallback callback) {
      this.callback = callback;
      this.factory = factory;
    }

    private <T> T callback(String property) {
      return (T)callback.onPropertyInvoked(this, factory.getProperty(property));
    }
    

    public ObjectProxyCallback getCallback() {
      return callback;
    }
    

    @Override
    public Building getBuilding() {
      return callback("building");
    }

    @Override
    public void setBuilding(Building value) {
      throw new UnsupportedOperationException();
    }

    @Override
    public UUID getId() {
      return callback("id");
    }

    @Override
    public void setId(UUID value) {
      throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
      return callback("name");
    }

    @Override
    public void setName(String value) {
      throw new UnsupportedOperationException();
    }

  }


  /**
  * The Property Factory
  */
  private static enum Property implements ObjectProperty {

    building(Type.from(Building.class), "building", true, "getBuilding", "setBuilding", new Class<?>[]{Building.class}),
    id(Type.from(UUID.class), "id", false, "getId", "setId", new Class<?>[]{UUID.class}),
    name(Type.from(String.class), "name", false, "getName", "setName", new Class<?>[]{String.class});

    private final String __name;
    private final Type<?> __propertyType;
    private final boolean __supportsReferences;
    private final Method __getGetMethodBinding;
    private final Method __getSetMethodBinding;

    Property(Type<?> propertyType, String name, boolean supportsReferences, String getMethodName, String setMethodName, Class<?>... args) {
      this.__name = name;
      this.__propertyType = propertyType;
      this.__supportsReferences = supportsReferences;
      this.__getGetMethodBinding = getMethodName != null ? getMethodBinding(getMethodName, new Class<?>[0]) : null;
      this.__getSetMethodBinding = setMethodName != null ? getMethodBinding(setMethodName, args) : null;
    }

    private Method getMethodBinding(String methodName, Class<?>... args) {
      try {
        return Office.class.getMethod(methodName, args);
      } catch (NoSuchMethodException ex) {
        throw new Error("Unable to find " + methodName + ". " + ex);
      }
    }

    @Override
    public String getName() {
      return __name;
    }

    @Override
    public Object get(Object instance) {
      return ((Office)instance).get(this);
    }

    @Override
    public void set(Object instance, Object value) {
      ((Office)instance).set(this, value);
    }

    @Override
    public String getDescription() {
      return null;
    }

    @Override
    public Method getGetMethodBinding() {
      return __getGetMethodBinding;
    }

    @Override
    public Type getType() {
       return __propertyType; 
    }

    @Override
    public boolean supportsReferences() {
      return __supportsReferences;
    }

    @Override
    public ValueReference<?> getReference(Object instance) {
      return ((Office)instance).getReference(this);
    }

    @Override
    public void setReference(Object instance, ValueReference<?> value) {
      ((Office)instance).setReference(this, value);
    }

    @Override
    public Method getSetMethodBinding() {
      return __getSetMethodBinding;
    }
  }

  private static final UUID versionUUID = java.util.UUID.fromString("92cf2bd6-13df-4d52-8c03-ae586e2e7822");

  private void writeObject(ObjectOutputStream out) throws IOException {
    out.writeObject(versionUUID);
    out.writeObject(getBuilding());
    out.writeObject(getId());
    out.writeObject(getName());
  }

  private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
    if (!versionUUID.equals((UUID)in.readObject())) {
      throw new InvalidClassException("Office version changed.");
    }
    if (this.building== null) {
      building= ValueReference.createDefault(Building.class);
    }
    setBuilding((Building)in.readObject());
    setId((UUID)in.readObject());
    setName((String)in.readObject());
  }

}
