package com.magnet.entities.remote;
import com.magnet.model.beans.ObjectFactory;
import com.magnet.model.beans.ObjectProperty;
import com.magnet.model.beans.ValueReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

public class EmployeeBuilder {

  private final Map<ObjectProperty, Object> values = new HashMap<ObjectProperty, Object>();
  private final ObjectFactory<Employee> factory = ObjectFactory.get(Employee.class);

  /**
   * Resets the set values in this builder
   */
  public void reset() {
    values.clear();
  }

  /**
   * Builds the target object given the values of the properties specified
  * @return The target object
   */
  public Employee build() {
    Employee instance = factory.create();
    for(Entry<ObjectProperty, Object> entry : values.entrySet()) {
      if(entry.getValue() instanceof ValueReference) {
        entry.getKey().setReference(instance, (ValueReference)entry.getValue());
      } else {
        entry.getKey().set(instance, entry.getValue());
      }
    }
    return instance;
  }

  /**
   * Sets the value of the name property
   * @param value The value of the property
   * @return The builder instance
   */
  public EmployeeBuilder name(String value) {
    values.put(factory.getProperty("name"), value);
    return this;
  }

  /**
   * Sets the value of the id property
   * @param value The value of the property
   * @return The builder instance
   */
  public EmployeeBuilder id(UUID value) {
    values.put(factory.getProperty("id"), value);
    return this;
  }

  /**
   * Sets the value of the department property
   * @param value The value of the property
   * @return The builder instance
   */
  public EmployeeBuilder department(Department value) {
    values.put(factory.getProperty("department"), value);
    return this;
  }

   /** 
   * Sets the value reference of the department property
   * @param value The value reference of the property
   * @return The builder instance
   */
  public EmployeeBuilder department(ValueReference<Department> value) {
    values.put(factory.getProperty("department"), value);
    return this;
  }

}
