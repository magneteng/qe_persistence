package com.magnet.entities.remote;
import com.magnet.model.beans.ObjectFactory;
import com.magnet.model.beans.ObjectProperty;
import com.magnet.model.beans.ValueReference;
import com.magnet.model.beans.ObjectProxyCallback;
import com.magnet.model.beans.ObjectFactoryReference;
import com.magnet.model.beans.spi.AbstractObjectFactory;
import com.magnet.model.reflection.Type;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.InvalidClassException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.UUID;
import com.magnet.persistence.RelativeCollection;

public class Department implements Serializable {
  static {
    try {
      ObjectFactory.register(new DepartmentFactory());
    } catch (NoSuchMethodException ex) {
      throw new Error("This cannot happen", ex);
    }
  }

  private String name;
  private RelativeCollection<Employee> employees;
  private UUID id;

  /**
  * Sets the name property
  * @param value The value of the name property
  */
  public void setName(String value) {
    this.name = value;
  }

  /**
  * @return The value of the name property
  */
  public String getName() {
    return name;
  }

  /**
  * @return The value of the employees property
  */
  public RelativeCollection<Employee> getEmployees() {
    return employees;
  }

  /**
  * Sets the id property
  * @param value The value of the id property
  */
  public void setId(UUID value) {
    this.id = value;
  }

  /**
  * @return The value of the id property
  */
  public UUID getId() {
    return id;
  }

  /**
  * Convert the instance to string
  */
  public String toString() {
    return "Department { " + "id : " + id + " }";
  }

  private Object[] hashCodeProperties() {
    return new Object[] { id, Department.class };
  }

  /**
  * @return hash code of the instance
  */
  public int hashCode() {
    return Arrays.deepHashCode(hashCodeProperties());
  }

  private Object[] equalsProperties() {
    return new Object[] { id };
  }

  /**
  * @return true if two instances are equal to each other; otherwise false
  */
  public boolean equals(Object paramObject) {
    return ((paramObject instanceof Department)) && ((this == paramObject) || (Arrays.deepEquals(equalsProperties(), ((Department)paramObject).equalsProperties())));
  }

  /**
  * Use in query for the property name
  */
  public static final String NAME = "name";

  /**
  * Use in query for the property id
  */
  public static final String ID = "id";


  /**
  * @param property The property
  * @return the value of the property
  */
  Object get(Property property) {
    switch (property) {
      case name:
        return getName();
      case employees:
        return getEmployees();
      case id:
        return getId();
      default:
        throw new Error("unknown property: " + property);
    }
  }

  /**
  * Sets the value of the property
  * @param property The property
  * @param value The value of the property
  */
  void set(Property property, Object value) {
    switch (property) {
      case name:
        setName((String) value);
        break;
      case employees:
        this.employees = (RelativeCollection<Employee>) value;
        break;
      case id:
        setId((UUID) value);
        break;
      default:
        throw new Error("unknown property: " + property);
    }
  }

  /**
  * @param property The property
  * @return the value reference of the property
  */
  ValueReference<?> getReference(Property property) {
    throw new Error("unknown property: ");
  }

  /**
  * Sets the value reference of the property
  * @param property The property
  * @param value The value reference of the property
  */
  void setReference(Property property, ValueReference<?> value) {
    throw new Error("unknown property: ");
  }


  /**
  * The Department Factory
  */
  private static class DepartmentFactory extends AbstractObjectFactory<Department> {

    public DepartmentFactory() throws NoSuchMethodException {
      super(null, Property.class.getEnumConstants());
    }

    @Override
    public Type<Department> getType() {
      return Type.from(Department.class);
    }

    @Override
    public Department create() {
      return new Department();
    }

    @Override
    public Department createProxy(ObjectProxyCallback callback) {
      return new DepartmentProxy(this, callback);
    }

    @Override
    public boolean supportsProxies() {
      return true;
    }

    @Override
    public ObjectProxyCallback getProxyCallback(Department proxy) throws UnsupportedOperationException, IllegalArgumentException {
      return ((DepartmentProxy) proxy).callback;
    }
  }


  /**
  * The Proxy of Department
  */
  @ObjectFactoryReference("com.magnet.entities.remote.Department")
  private static class DepartmentProxy extends Department {

    private final ObjectProxyCallback callback;
    private final ObjectFactory<?> factory;

    public DepartmentProxy(ObjectFactory<?> factory, ObjectProxyCallback callback) {
      this.callback = callback;
      this.factory = factory;
    }

    private <T> T callback(String property) {
      return (T)callback.onPropertyInvoked(this, factory.getProperty(property));
    }
    

    public ObjectProxyCallback getCallback() {
      return callback;
    }
    

    @Override
    public String getName() {
      return callback("name");
    }

    @Override
    public void setName(String value) {
      throw new UnsupportedOperationException();
    }

    @Override
    public RelativeCollection<Employee> getEmployees() {
      return callback("employees");
    }

    @Override
    public UUID getId() {
      return callback("id");
    }

    @Override
    public void setId(UUID value) {
      throw new UnsupportedOperationException();
    }

  }


  /**
  * The Property Factory
  */
  private static enum Property implements ObjectProperty {

    name(Type.from(String.class), "name", false, "getName", "setName", new Class<?>[]{String.class}),
    employees(Type.build(RelativeCollection.class, Employee.class), "employees", false, "getEmployees", null, new Class<?>[]{RelativeCollection.class}),
    id(Type.from(UUID.class), "id", false, "getId", "setId", new Class<?>[]{UUID.class});

    private final String __name;
    private final Type<?> __propertyType;
    private final boolean __supportsReferences;
    private final Method __getGetMethodBinding;
    private final Method __getSetMethodBinding;

    Property(Type<?> propertyType, String name, boolean supportsReferences, String getMethodName, String setMethodName, Class<?>... args) {
      this.__name = name;
      this.__propertyType = propertyType;
      this.__supportsReferences = supportsReferences;
      this.__getGetMethodBinding = getMethodName != null ? getMethodBinding(getMethodName, new Class<?>[0]) : null;
      this.__getSetMethodBinding = setMethodName != null ? getMethodBinding(setMethodName, args) : null;
    }

    private Method getMethodBinding(String methodName, Class<?>... args) {
      try {
        return Department.class.getMethod(methodName, args);
      } catch (NoSuchMethodException ex) {
        throw new Error("Unable to find " + methodName + ". " + ex);
      }
    }

    @Override
    public String getName() {
      return __name;
    }

    @Override
    public Object get(Object instance) {
      return ((Department)instance).get(this);
    }

    @Override
    public void set(Object instance, Object value) {
      ((Department)instance).set(this, value);
    }

    @Override
    public String getDescription() {
      return null;
    }

    @Override
    public Method getGetMethodBinding() {
      return __getGetMethodBinding;
    }

    @Override
    public Type getType() {
       return __propertyType; 
    }

    @Override
    public boolean supportsReferences() {
      return __supportsReferences;
    }

    @Override
    public ValueReference<?> getReference(Object instance) {
      return ((Department)instance).getReference(this);
    }

    @Override
    public void setReference(Object instance, ValueReference<?> value) {
      ((Department)instance).setReference(this, value);
    }

    @Override
    public Method getSetMethodBinding() {
      return __getSetMethodBinding;
    }
  }

  private static final UUID versionUUID = java.util.UUID.fromString("13fb1751-8a87-48da-a839-451a13abd81a");

  private void writeObject(ObjectOutputStream out) throws IOException {
    out.writeObject(versionUUID);
    out.writeObject(getName());
    out.writeObject(getId());
  }

  private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
    if (!versionUUID.equals((UUID)in.readObject())) {
      throw new InvalidClassException("Department version changed.");
    }
    setName((String)in.readObject());
    setId((UUID)in.readObject());
  }

}
