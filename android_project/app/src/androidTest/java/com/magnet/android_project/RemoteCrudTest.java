package com.magnet.android_project;

import android.content.Context;
import android.net.Uri;
import android.test.ActivityTestCase;
import android.util.Log;

import com.magnet.android.mms.MagnetMobileClient;
import com.magnet.android.mms.RemoteDataService;
import com.magnet.android.mms.connection.ConnectionConfigManager;
import com.magnet.android.mms.connection.MagnetRestAuthHandler;
import com.magnet.entities.remote.*;
import com.magnet.persistence.EntityContextType;
import com.magnet.persistence.services.ConnectionStatus;
import com.magnet.persistence.services.ConnectionStatusProvider;
import com.magnet.persistence.services.EntityContextRuntimeFactory;
import com.magnet.persistence.services.UserProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by tnguyen on 11/26/14.
 */
public class RemoteCrudTest extends ActivityTestCase {
    private static final String TEST_HOST = "http://192.168.101.158:8090";
    public static final String TEST_CONFIG_NAME = "DataServer";

    private MagnetMobileClient magnetMobileClient;
    private ConnectionConfigManager.ConnectionConfig connectionConfig;
    private AndroidDataClient androidDataClient;
    private RemoteDataService remoteDataService;

    public static ConnectionConfigManager.ConnectionConfig getConnectionConfig(MagnetMobileClient magnetClient) {
        // load ConnectionConfig
        ConnectionConfigManager cm = magnetClient.getManager(ConnectionConfigManager.class, magnetClient.getAppContext());
        ConnectionConfigManager.ConnectionConfig connection = cm.addOrReplaceConnectionConfig(TEST_CONFIG_NAME, Uri.parse(TEST_HOST),
                ConnectionConfigManager.ConnectionConfig.ConfigType.MAGNET_REST, MagnetRestAuthHandler.class, "magnet");
        connection.setToken(ConnectionConfigManager.AuthHandler.TokenType.USERNAME, "admin", true);
        connection.setToken(ConnectionConfigManager.AuthHandler.TokenType.PASSWORD, "admin", true);

        return connection;
    }

    public void setUp() throws Exception {
        Context myContext = this.getInstrumentation().getTargetContext();
        magnetMobileClient = MagnetMobileClient.getInstance(myContext);
        connectionConfig = getConnectionConfig(magnetMobileClient);
        androidDataClient = new AndroidDataClient(myContext,
                null,
                myContext.getResources().getAssets().open("persistence/RDM.conf"));
        remoteDataService = new RemoteDataService(connectionConfig, androidDataClient);
        com.magnet.android.mms.utils.logger.Log.setLoggable(com.magnet.android.mms.utils.logger.Log.VERBOSE);
    }

    private List<Department> readDepartment(AndroidDataClient androidDataClient) {
        List<Department> resultList = new ArrayList<Department>();
        EntityContext entityContext = androidDataClient.createEntityContext(EntityContextType.READ_ONLY,
        this.remoteDataService.getRemoteServicesContainer());
        resultList = entityContext.getDepartments().orderByName(Department.NAME).executeList();

        entityContext.close();
        return resultList;
    }

    private List<Employee> readEmployee(AndroidDataClient androidDataClient) {
        List<Employee> resultList = new ArrayList<Employee>();
        EntityContext entityContext = androidDataClient.createEntityContext(EntityContextType.READ_ONLY,
                this.remoteDataService.getRemoteServicesContainer());
        resultList = entityContext.getEmployees().orderByName(Employee.NAME).executeList();

        entityContext.close();
        return resultList;
    }

    public void test01RemoteProviderTest() throws Exception {
        assertNotNull(remoteDataService.getRemoteServicesContainer().getService(EntityContextRuntimeFactory.class));
        UserProvider userProvider = remoteDataService.getRemoteServicesContainer().getService(UserProvider.class);
        assertNotNull(userProvider);
        assertEquals(connectionConfig.getToken(ConnectionConfigManager.AuthHandler.TokenType.USERNAME), userProvider.getCurrentUser());
        ConnectionStatusProvider connProvider = remoteDataService.getRemoteServicesContainer().getService(ConnectionStatusProvider.class);
        assertNotNull(connProvider);
        assertEquals(ConnectionStatus.CONNECTED, connProvider.getConnectionStatus());
    }

    public void test02DepartmentCreateTest() {
        EntityContext createContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE,
                remoteDataService.getRemoteServicesContainer());

        // Create
        DepartmentBuilder departmentBuilder = new DepartmentBuilder();
        departmentBuilder.id(UUID.randomUUID()).name("department1");
        createContext.add(departmentBuilder.build());
        createContext.flush();
        createContext.close();

        // Read
        List<Department> departments = this.readDepartment(this.androidDataClient);
        Log.d("^^^^^^^ number of departments after create: ", departments.size() + "");
        assertTrue("Only One Department should have been created", departments.size() == 1);
        assertEquals("Department with name 'department1' should have been created", "department1", departments.get(0).getName());
    }

    public void test03DepartmentUpdateTest() {
        // Update
        List<Department> departments = this.readDepartment(this.androidDataClient);
        Department updatedDepartment = departments.get(0);
        updatedDepartment.setName("building1_new_name");

        EntityContext updateContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE,
                this.remoteDataService.getRemoteServicesContainer());
        updateContext.attachAsUpdated(updatedDepartment);
        updateContext.flush();
        updateContext.close();

        // Read
        departments.clear();
        departments = this.readDepartment(this.androidDataClient);
        Log.d("^^^^^^^ number of buildings after update: ", departments.size() + "");
        assertTrue("No new Department should have been created", departments.size() == 1);
        assertEquals("Building with name 'building1_new_name' should have been updated", "building1_new_name",
                departments.get(0).getName());
    }

    public void test04DepartmentDeleteTest() {
        // Delete
        List<Department> departments = this.readDepartment(this.androidDataClient);
        Department deleteDepartment = departments.get(0);
        EntityContext deleteContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE,
                this.remoteDataService.getRemoteServicesContainer());

        deleteContext.attach(deleteDepartment);
        deleteContext.delete(deleteDepartment);
        deleteContext.flush();
        deleteContext.close();

        // Read
        departments.clear();
        departments = this.readDepartment(this.androidDataClient);
        Log.d("^^^^^^^ number of Department after delete: ", departments.size()+"");
        assertTrue("Department should have been deleted", departments.size() == 0);
    }

    public void test05OEmployeeCreateTest() {
        EntityContext createContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE,
                this.remoteDataService.getRemoteServicesContainer());

        // Create
        EmployeeBuilder employeeBuilder = new EmployeeBuilder();
        employeeBuilder.id(UUID.randomUUID());
        employeeBuilder.name("employee1");
        createContext.add(employeeBuilder.build());
        createContext.flush();
        createContext.close();

        // Read
        List<Employee> employees = this.readEmployee(this.androidDataClient);
        Log.d("^^^^^^^ number of employees after create: ", employees.size() + "");
        assertTrue("Only One Employee should have been created", employees.size() == 1);
        assertEquals("Employee with name 'employee1' should have been created", "employee1", employees.get(0).getName());
    }

    public void test06EmployeeUpdateTest() {
        // Update
        List<Employee> employees = this.readEmployee(this.androidDataClient);
        Employee updatedEmployee = employees.get(0);
        updatedEmployee.setName("employee1_new_name");

        EntityContext updateContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE,
                this.remoteDataService.getRemoteServicesContainer());
        updateContext.attachAsUpdated(updatedEmployee);
        updateContext.flush();
        updateContext.close();

        // Read
        employees = this.readEmployee(this.androidDataClient);
        Log.d("^^^^^^^ number of buildings after create: ", employees.size() + "");
        assertTrue("No new employee should have been created", employees.size() == 1);
        assertEquals("Employee with name 'employee1_new_name' should have been updated", "employee1_new_name", employees.get(0).getName());
    }

    public void test07EmployeeDeleteTest() {
        // Delete
        List<Employee> employees = this.readEmployee(this.androidDataClient);
        Employee deleteEmployee = employees.get(0);
        EntityContext deleteContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE,
                this.remoteDataService.getRemoteServicesContainer());

        deleteContext.attach(deleteEmployee);
        deleteContext.delete(deleteEmployee);
        deleteContext.flush();
        deleteContext.close();

        // Read
        employees = this.readEmployee(this.androidDataClient);
        Log.d("^^^^^^^ number of employee after create: ", employees.size()+"");
        assertTrue("Employee should have been deleted", employees.size() == 0);
    }
}
