package com.magnet.android_project;

import android.content.Context;
import android.test.ActivityTestCase;
import android.util.Log;

import com.magnet.entities.local.*;
import com.magnet.persistence.EntityContextType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LocalCrudTest extends ActivityTestCase {
    private AndroidDataClient androidDataClient;

    public void setUp() throws Exception {
        Context myContext = this.getInstrumentation().getTargetContext();
        androidDataClient = new AndroidDataClient(myContext,
                null,
                myContext.getResources().getAssets().open("persistence/LDM.conf"));
        assertNotNull(androidDataClient);
    }

    private List<Building> readBuilding(AndroidDataClient androidDataClient) {
        List<Building> resultList = new ArrayList<Building>();
        EntityContext entityContext = androidDataClient.createEntityContext(EntityContextType.READ_ONLY);
        resultList = entityContext.getBuildings().orderByName(Building.NAME).executeList();

        entityContext.close();
        return resultList;
    }

    private List<Office> readOffice(AndroidDataClient androidDataClient) {
        List<Office> resultList = new ArrayList<Office>();
        EntityContext entityContext = androidDataClient.createEntityContext(EntityContextType.READ_ONLY);
        resultList = entityContext.getOffices().orderByName(Office.NAME).executeList();

        entityContext.close();
        return resultList;
    }

    public void test01BuildingCreateTest() {
        EntityContext createContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);

        // Create
        BuildingBuilder building = new BuildingBuilder();
        building.id(UUID.randomUUID()).name("building1");
        createContext.add(building.build());
        createContext.flush();
        createContext.close();

        // Read
        List<Building> buildings = this.readBuilding(this.androidDataClient);
        Log.d("^^^^^^^ number of buildings after create: ", buildings.size() + "");
        assertTrue("Only One Building should have been created", buildings.size() == 1);
        assertEquals("Building with name 'building1' should have been created", "building1", buildings.get(0).getName());
    }

    public void test02BuildingUpdateTest() {
        this.test01BuildingCreateTest();
        //EntityContext createContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);

        // Update
        List<Building> buildings = this.readBuilding(this.androidDataClient);
        Building updatedBuilding = buildings.get(0);
        updatedBuilding.setName("building1_new_name");

        EntityContext updateContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);
        updateContext.attachAsUpdated(updatedBuilding);
        updateContext.flush();
        updateContext.close();

        // Read
        buildings.clear();
        buildings = this.readBuilding(this.androidDataClient);
        Log.d("^^^^^^^ number of buildings after update: ", buildings.size() + "");
        assertTrue("Only One Building should have been created", buildings.size() == 1);
        assertEquals("Building with name 'building1_new_name' should have been updated", "building1_new_name",
                buildings.get(0).getName());
    }

    public void test03BuildingDeleteTest() {
        this.test01BuildingCreateTest();
        //EntityContext createContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);

        // Delete
        List<Building> buildings = this.readBuilding(this.androidDataClient);
        Building deleteBuilding = buildings.get(0);
        EntityContext deleteContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);

        deleteContext.attach(deleteBuilding);
        deleteContext.delete(deleteBuilding);
        deleteContext.flush();
        deleteContext.close();

        // Read
        buildings.clear();
        buildings = this.readBuilding(this.androidDataClient);
        Log.d("^^^^^^^ number of buildings after delete: ", buildings.size()+"");
        assertTrue("Building should have been deleted", buildings.size() == 0);
    }

    public void test04OfficeCreateTest() {
        EntityContext createContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);

        // Create
        OfficeBuilder officeBuilder = new OfficeBuilder();
        officeBuilder.id(UUID.randomUUID());
        officeBuilder.name("office1");
        createContext.add(officeBuilder.build());
        createContext.flush();
        createContext.close();

        // Read
        List<Office> offices = this.readOffice(this.androidDataClient);
        Log.d("^^^^^^^ number of buildings after create: ", offices.size() + "");
        assertTrue("Only One Building should have been created", offices.size() == 1);
        assertEquals("Office with name 'office1' should have been created", "office1", offices.get(0).getName());
    }

    public void test04OfficeUpdateTest() {
        this.test04OfficeCreateTest();
        //EntityContext createContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);

        // Update
        List<Office> offices = this.readOffice(this.androidDataClient);
        Office updatedOffice = offices.get(0);
        updatedOffice.setName("office1_new_name");

        EntityContext updateContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);
        updateContext.attachAsUpdated(updatedOffice);
        updateContext.flush();
        updateContext.close();

        // Read
        offices = this.readOffice(this.androidDataClient);
        Log.d("^^^^^^^ number of buildings after create: ", offices.size() + "");
        assertTrue("Only One Building should have been created", offices.size() == 1);
        assertEquals("Office with name 'office1_new_name' should have been updated", "office1_new_name", offices.get(0).getName());
    }

    public void test06OfficeDeleteTest() {
        this.test04OfficeCreateTest();
        //EntityContext createContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);

        // Delete
        List<Office> offices = this.readOffice(this.androidDataClient);
        Office deleteOffice = offices.get(0);
        EntityContext deleteContext = androidDataClient.createEntityContext(EntityContextType.READ_WRITE);

        deleteContext.attach(deleteOffice);
        deleteContext.delete(deleteOffice);
        deleteContext.flush();
        deleteContext.close();

        // Read
        offices = this.readOffice(this.androidDataClient);
        Log.d("^^^^^^^ number of buildings after create: ", offices.size()+"");
        assertTrue("Office should have been deleted", offices.size() == 0);
    }
}
